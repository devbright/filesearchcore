﻿using System.Collections.Generic;
using Infrastructure.SearchModels;

namespace Infrastructure.Interfaces.DataParser
{
    public interface IDataParser
    {
        string Type { get; }
        IEnumerable<SearchItem> Parse(string path);

        IEnumerable<SearchItem> ParseMany(string directoryPath);
    }
}
