﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Infrastructure.Interfaces.DataParser;
using Infrastructure.SearchModels;

namespace FileSearch.Core.DataParser
{
    public class WordDocParser : IDataParser
    {
        public string Type => ".docx";
        public IEnumerable<SearchItem> Parse(string path)
        {
            using (var file = WordprocessingDocument.Open(path, false))
            {
                var builder = new StringBuilder();
                var element = file.MainDocumentPart.Document.Body;
                if (element == null)
                {
                    return null;
                }

                builder.Append(GetPlainText(element));
                return new List<SearchItem>
                {
                    new SearchItem()
                    {
                        Id = Guid.NewGuid(),
                        FileName = Path.GetFileName(path),
                        Body = builder.ToString()
                    }
                };
            }
        }

        public IEnumerable<SearchItem> ParseMany(string directoryPath)
        {
            var items = new List<SearchItem>();
            var files = Directory.GetFiles(directoryPath);
            if (!files.ToList().Any())
                return items;

            foreach (var file in files)
            {
                items.AddRange(Parse(file));
            }
            return items;
        }

        private string GetPlainText(OpenXmlElement element)
        {
            var builder = new StringBuilder();
            foreach (var section in element.Elements())
            {
                switch (section.LocalName)
                {
                    case "t":
                        builder.Append(section.InnerText);
                        break;

                    case "cr":
                    case "br":
                        builder.Append(Environment.NewLine);
                        break;

                    case "tab":
                        builder.Append("\t");
                        break;

                    case "p":
                        builder.Append(GetPlainText(section));
                        builder.AppendLine(Environment.NewLine);
                        break;

                    default:
                        builder.Append(GetPlainText(section));
                        break;
                }
            }
            return builder.ToString();
        }
    }
}
