﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Interfaces.DataParser;
using Infrastructure.SearchModels;

namespace FileSearch.Core.DataParser
{
    public class TextFileParser : IDataParser
    {
        public string Type => ".txt";

        public IEnumerable<SearchItem> Parse(string path)
        {
            return new List<SearchItem>()
            {
                new SearchItem()
                {
                    Id = Guid.NewGuid(),
                    FileName = Path.GetFileName(path),
                    Body = GetFileBody(path)
                }
            };
        }

        public IEnumerable<SearchItem> ParseMany(string directoryPath)
        {
            var files = Directory.GetFiles(directoryPath);
            if(!files.ToList().Any())
                yield return new SearchItem();

            foreach (var file in files)
            {
                yield return new SearchItem()
                {
                    Id = Guid.NewGuid(),
                    FileName = Path.GetFileName(file),
                    Body = GetFileBody(file)
                };
            }
        }

        private string GetFileBody(string filePath)
        {
            string line;
            var builder = new StringBuilder();

            var file = new System.IO.StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                builder.AppendLine(line);
            }

            file.Close();
            return builder.ToString();
        }
    }
}
