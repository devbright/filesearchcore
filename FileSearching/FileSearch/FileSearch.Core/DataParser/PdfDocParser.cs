﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Infrastructure.Interfaces.DataParser;
using Infrastructure.SearchModels;

namespace FileSearch.Core.DataParser
{
    public class PdfDocParser : IDataParser
    {
        public string Type => ".pdf";

        public IEnumerable<SearchItem> Parse(string path)
        {
            using (var reader = new PdfReader(path))
            {
                var text = new StringBuilder();

                for (var page = 1; page <= reader.NumberOfPages; page++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, page));
                }

                return new List<SearchItem>
                {
                    new SearchItem()
                    {
                        Id = Guid.NewGuid(),
                        FileName = System.IO.Path.GetFileName(path),
                        Body = text.ToString()
                    }
                };
            }
        }

        public IEnumerable<SearchItem> ParseMany(string directoryPath)
        {
            var items = new List<SearchItem>();
            var files = System.IO.Directory.GetFiles(directoryPath);
            if (!files.ToList().Any())
                return items;

            foreach (var file in files)
            {
                items.AddRange(Parse(file));
            }
            return items;
        }
    }
}
