﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Interfaces.DataParser;
using Infrastructure.SearchModels;

namespace FileSearch.Core.DataParser
{
    public class GenericDataParser : IDataParser
    {
        private readonly List<IDataParser> _parsers;

        public string Type => "*";

        public GenericDataParser(List<IDataParser> parsers)
        {
            _parsers = parsers;
        }

        public IEnumerable<SearchItem> Parse(string path)
        {
            return _parsers.FirstOrDefault(x => x.Type == Path.GetExtension(path))?.Parse(path);
        }

        public IEnumerable<SearchItem> ParseMany(string directoryPath)
        {
            var items = new List<SearchItem>();
            var files = Directory.GetFiles(directoryPath);
            if (!files.ToList().Any())
                return items;

            foreach (var file in files)
            {
                items.AddRange(Parse(file));
            }
            return items;
        }
    }
}
