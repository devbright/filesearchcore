﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Infrastructure.Interfaces.DataParser;
using Infrastructure.SearchModels;

namespace FileSearch.Core.DataParser
{
    public class SiteMapParser : IDataParser
    {
        public string Type => ".smap";
        public IEnumerable<SearchItem> Parse(string uri)
        {
            var items = new List<SearchItem>();
            string line;
            var file = new System.IO.StreamReader(uri);
            while ((line = file.ReadLine()) != null)
                items.AddRange(ParseSiteMap(line));

            file.Close();
            return items;
        }

        public IEnumerable<SearchItem> ParseMany(string directoryPath)
        {
            var items = new List<SearchItem>();
            var files = Directory.GetFiles(directoryPath);
            if (!files.ToList().Any())
                return items;

            foreach (var file in files)
            {
                items.AddRange(Parse(file));
            }
            return items;
        }

        public IEnumerable<SearchItem> ParseSiteMap(string uri)
        {
            var items = new List<SearchItem>();
            var request = WebRequest.Create(uri);
            var response = request.GetResponse();
            var data = response.GetResponseStream();
            if (data == null) return items;
            using (var xml = new StreamReader(data))
            {
                var doc = new XmlDocument();
                var xmlStream = xml.ReadToEnd();
                doc.LoadXml(xmlStream);
                var elements = doc.GetElementsByTagName("loc");
                for (var element = 0; element < elements.Count; element++)
                {
                    items.Add(ReadHtml(elements[element].InnerText));
                }
            }
            return items;
        }

        private SearchItem ReadHtml(string uri)
        {
            var request = WebRequest.Create(uri);
            var response = request.GetResponse();
            var data = response.GetResponseStream();
            if (data == null) return null;
            using (var html = new StreamReader(data))
            {
                return new SearchItem()
                {
                    Id = Guid.NewGuid(),
                    FileName = uri,
                    Body = RemoveHtmlTags(html.ReadToEnd())
                };
            }
        }

        private string RemoveHtmlTags(string html)
        {
            var regex = new Regex(
                "(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)",
                RegexOptions.Singleline | RegexOptions.IgnoreCase
            );

            return Regex.Replace(regex.Replace(html, string.Empty), "<.*?>", string.Empty);
        }
    }
}
