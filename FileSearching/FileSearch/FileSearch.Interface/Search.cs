﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSearch.Core.Search;

namespace FileSearch.Interface
{
    public static class Search
    {
        public static void Files(LuceneSearch searcher)
        {
            var stop = true;
            do
            {
                Console.WriteLine("Input Search Term: Press Enter To End Search");
                var input = Console.ReadLine();
                if (input == "")
                    stop = false;
                else
                {
                    Console.Clear();
                    Console.WriteLine($"Search Term {input}");
                    Console.WriteLine("Result:");
                    var results = searcher.Search(input).ToList();
                    if (!results.Any())
                    {
                        Console.WriteLine("No Result");
                    }
                    else
                    {
                        foreach (var result in results)
                        {
                            Console.WriteLine("-------------------------------");
                            Console.WriteLine($"Id: {result.Id}");
                            Console.WriteLine($"File Name: {result.FileName}");
                            //Console.WriteLine($"Body: {result.Body}");

                        }
                    }
                    Console.WriteLine("\nPress Enter To Seach Again");
                    Console.ReadKey();
                    Console.Clear();

                }
            } while (stop);
        }
    }
}
