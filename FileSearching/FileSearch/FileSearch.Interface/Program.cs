﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSearch.Core.DataParser;
using FileSearch.Core.Search;
using Infrastructure.Interfaces.DataParser;

namespace FileSearch.Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            var luceneSearch = new LuceneSearch("C:/GIT/FileSearching/LucenceIndex");
            var parser = new GenericDataParser(new List<IDataParser> {new TextFileParser(), new PdfDocParser(), new WordDocParser(), new SiteMapParser()});
            luceneSearch.AddUpdateLuceneIndex(parser.ParseMany("C:/GIT/filesearchcore/FileSearching/TestFiles"));

            var stop = true;
            do
            {
                Console.Clear();
                Console.WriteLine("1: Add New File \n2: Search \n3: End");
                var input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        Console.WriteLine("Input File Path");
                        var result = parser.Parse(Console.ReadLine());
                        if (result != null)
                        {
                            luceneSearch.AddUpdateLuceneIndex(result);
                            luceneSearch.Optimize();
                        }
                        break;
                    case "2":
                        Search.Files(luceneSearch);
                        break;
                    case "3":
                        stop = false;
                        break;
                    default:
                        Console.WriteLine("Invaild Option");
                        break;
                }
            } while (stop);

            luceneSearch.ClearLuceneIndex();
        }
    }
}
